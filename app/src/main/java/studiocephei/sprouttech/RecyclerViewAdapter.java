package studiocephei.sprouttech;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

/**
 * Created by Matt on 7/09/2015.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private  LayoutInflater inflater;
    List<Information> data = Collections.emptyList();
    private  Context context;

    public RecyclerViewAdapter(Context context, List<Information> data){
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_view, parent, false);
        ItemHolder holder = new ItemHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemHolder itemHolder = (ItemHolder) holder;
        Information current = data.get(position);
        itemHolder.title.setText(current.title);
        itemHolder.image.setImageResource(current.iconId);
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView title;
        ImageView image;
        AccountActivity accountActivity;

        public ItemHolder(View itemView) {
            super(itemView);
            accountActivity = (AccountActivity)context;
            title = (TextView) itemView.findViewById(R.id.txt_txt);
            image = (ImageView) itemView.findViewById(R.id.iv_img);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {

            switch (getPosition()){
                case 0:
                    Log.d(Constants.SPROUT_TAG, "Account");
                    break;
                case 1:
                    accountActivity.openInvestmentActivity();
                    break;
                case 2:
                    Log.d(Constants.SPROUT_TAG, "Technical");
                    break;
            }

        }



    }
}
