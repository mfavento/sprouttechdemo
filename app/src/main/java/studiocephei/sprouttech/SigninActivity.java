package studiocephei.sprouttech;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.facebook.FacebookSdk;
import com.facebook.login.widget.LoginButton;


public class SigninActivity extends Activity {

    private LoginButton mLoginButton;
    private Button mNextBtn;
    private EditText mEmailEt;
    private EditText mPasswordEt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_signin);
        mLoginButton = (LoginButton) findViewById(R.id.login_button);
        mNextBtn = (Button) findViewById(R.id.btn_next);
        //dev command
        mEmailEt = (EditText) findViewById(R.id.et_email);
        mPasswordEt = (EditText) findViewById(R.id.et_password);
        mEmailEt.setHint("Email");
        mPasswordEt.setHint("Password");

        attachBtn();
    }

    private void attachBtn(){
        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAccountActivity();
            }
        });

    }

    private void openAccountActivity(){
        Intent i = new Intent(this, AccountActivity.class);
        startActivity(i);
    }

}
