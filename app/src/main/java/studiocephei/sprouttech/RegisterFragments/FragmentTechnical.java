package studiocephei.sprouttech.RegisterFragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import studiocephei.sprouttech.Constants;
import studiocephei.sprouttech.InvestmentActivity;
import studiocephei.sprouttech.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 */
public class FragmentTechnical extends Fragment {

    ImageView mImageView;
    Bitmap mBitStable;
    Bitmap mBitStay;
    Bitmap mBitSlow;
    Bitmap mBitSurge;
    InvestmentActivity mActivityInvest;



    public FragmentTechnical() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_technical, container, false);
        mImageView = (ImageView)v.findViewById(R.id.iv_image);
        mBitStay = Constants.decodeSampledBitmapFromResource(getResources(), R.drawable.stay_graph, 400, 400);
        mBitSlow = Constants.decodeSampledBitmapFromResource(getResources(),R.drawable.slow_graph, 400, 400);
        mBitStable = Constants.decodeSampledBitmapFromResource(getResources(),R.drawable.stable_graph, 400, 400);
        mBitSurge = Constants.decodeSampledBitmapFromResource(getResources(),R.drawable.surge_graph, 400, 400);
        mActivityInvest = (InvestmentActivity) getActivity();
        mImageView = (ImageView)v.findViewById(R.id.iv_image);
        //get data from investment activity
        return v;
    }




    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public static FragmentTechnical newInstance() {
        FragmentTechnical fragmentTechnical = new FragmentTechnical();
        return fragmentTechnical;
    }

    public void update(int progress){
        switch (progress){
            case 0:
                mImageView.setImageBitmap(mBitStay);
                break;
            case 1:
                mImageView.setImageBitmap(mBitSlow);
                break;
            case 2:
                mImageView.setImageBitmap(mBitStable);
                break;
            case 3:
                mImageView.setImageBitmap(mBitSurge);
                break;
        }
    }

}
