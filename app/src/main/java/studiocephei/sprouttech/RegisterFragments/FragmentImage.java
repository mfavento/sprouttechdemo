package studiocephei.sprouttech.RegisterFragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import studiocephei.sprouttech.Constants;
import studiocephei.sprouttech.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 */
public class FragmentImage extends Fragment {

    ImageView mImageView;
    Bitmap mBitStable;
    Bitmap mBitStay;
    Bitmap mBitSlow;
    Bitmap mBitSurge;






    public FragmentImage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_image, container, false);
        mImageView = (ImageView)v.findViewById(R.id.iv_image);
        mBitStay = Constants.decodeSampledBitmapFromResource(getResources(), R.drawable.stay_sprout, 800, 800);
        mBitSlow = Constants.decodeSampledBitmapFromResource(getResources(),R.drawable.slow_sprout, 800, 800);
        mBitStable = Constants.decodeSampledBitmapFromResource(getResources(),R.drawable.stable_sprout, 800, 800);
        mBitSurge = Constants.decodeSampledBitmapFromResource(getResources(),R.drawable.surge_sprout, 800, 800);

        mImageView.setImageBitmap(mBitStay);
        //get data from investment activity
        return v;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public static FragmentImage newInstance() {
        FragmentImage fragmentImage = new FragmentImage();
        return fragmentImage;
    }

    public void update(int progress){
        switch (progress){
            case 0:
                mImageView.setImageBitmap(mBitStay);
                break;
            case 1:
                mImageView.setImageBitmap(mBitSlow);
                break;
            case 2:
                mImageView.setImageBitmap(mBitStable);
                break;
            case 3:
                mImageView.setImageBitmap(mBitSurge);
                break;
        }
    }


}
