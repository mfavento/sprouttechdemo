package studiocephei.sprouttech;


import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;

import studiocephei.sprouttech.AccountFragments.FragmentAccImage;
import studiocephei.sprouttech.AccountFragments.FragmentAccPerformance;
import studiocephei.sprouttech.AccountFragments.FragmentAccPortfolio;
import studiocephei.sprouttech.RegisterFragments.FragmentImage;
import studiocephei.sprouttech.RegisterFragments.FragmentSuperFind;
import studiocephei.sprouttech.RegisterFragments.FragmentTechnical;
import studiocephei.sprouttech.Tabs.SlidingTabLayout;


public class AccountActivity extends AppCompatActivity
{

    private Toolbar toolbar;
    public int mChoice;
    private ViewPager mPager;
    private SlidingTabLayout mTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        setTitle("Account Overview");
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));


        mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mTabs.setViewPager(mPager);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                updateImage(mChoice);

            }
        });


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);

        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout)findViewById(R.id.drawer_layout), toolbar);
    }

    public void updateImage(int progress){
        //finds current fragment
        Fragment frag = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + mPager.getCurrentItem());
        //image fragment
        mChoice = progress;
        //((FragmentAccImage)frag).update(mChoice);


    }

    public void openInvestmentActivity(){
        Intent i = new Intent(this, InvestmentActivity.class);
        startActivity(i);
    }




    class MyPagerAdapter extends FragmentPagerAdapter {

        String[] tabs;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
            tabs = getResources().getStringArray(R.array.tabs_acc);
        }

        @Override
        public Fragment getItem(int position) {
             switch(position) {
                 case 0:return FragmentAccImage.newInstance();
                 case 1:return FragmentAccPerformance.newInstance();
                 case 2:return FragmentAccPortfolio.newInstance();
                 default: return FragmentImage.newInstance();

             }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }

        @Override
        public int getCount() {
            return 3;
        }

        public void update(){

        }
    }

}