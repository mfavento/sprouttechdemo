package studiocephei.sprouttech;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Menu;

import studiocephei.sprouttech.RegisterFragments.FragmentAge;
import studiocephei.sprouttech.RegisterFragments.FragmentGlobalGrowth;
import studiocephei.sprouttech.RegisterFragments.FragmentImportance;
import studiocephei.sprouttech.RegisterFragments.FragmentPlant;
import studiocephei.sprouttech.RegisterFragments.FragmentSalary;
import studiocephei.sprouttech.RegisterFragments.FragmentStock;
import studiocephei.sprouttech.RegisterFragments.FragmentSuperFind;
import studiocephei.sprouttech.RegisterFragments.FragmentSupers;

/**
 * This is the main activity of my app, the bulk of my fragment methods are done from here*
 */

public class HomeActivity extends FragmentActivity
{
    private int mContainerRes;

    //gets current user
   // ParseUser mCurrentUser = ParseUser.getCurrentUser();


    /**
     * Temporary override of the back button so the user can't go back to the login screen and stuff with how the loginButton queries
     */

    @Override
    public void onBackPressed()
    {
        //No going back to login screen without logging out
    }


    /**
     * Sets up the drawer fragment and runs a 'setupDrawerConfiguration'
     * Default open view to user's profile
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setActionBarTitle("Account Overview");
        FragmentSuperFind fragmentSuperFind = new FragmentSuperFind();
        mContainerRes = R.id.fragment_container;
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(mContainerRes, fragmentSuperFind).commit();

    }

    public void switchToWizard(){
        FragmentSuperFind fragmentSuperFind = new FragmentSuperFind();
        FragmentManager fragmentManager = getSupportFragmentManager();
        setActionBarTitle("Find My Super");
        fragmentManager.beginTransaction().replace(mContainerRes, fragmentSuperFind).commit();
    }

    public void switchToSuper(){
        FragmentSupers fragmentSupers = new FragmentSupers();
        FragmentManager fragmentManager = getSupportFragmentManager();
        setActionBarTitle("Found Supers");
        fragmentManager.beginTransaction().replace(mContainerRes, fragmentSupers).commit();
    }

    public void switchToPlant(){
        FragmentPlant fragmentPlant = new FragmentPlant();
        FragmentManager fragmentManager = getSupportFragmentManager();
        setActionBarTitle("Your Sprout");
        fragmentManager.beginTransaction().replace(mContainerRes, fragmentPlant).commit();
    }

    public void switchToAge(){
        FragmentAge fragmentAge = new FragmentAge();
        FragmentManager fragmentManager = getSupportFragmentManager();
        setActionBarTitle("Invest");
        fragmentManager.beginTransaction().replace(mContainerRes, fragmentAge).commit();
    }

    public void switchToGrowth(){
        FragmentGlobalGrowth fragmentGlobalGrowth = new FragmentGlobalGrowth();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(mContainerRes, fragmentGlobalGrowth).commit();
    }

    public void switchToSalary(){
        FragmentSalary fragmentSalary = new FragmentSalary();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(mContainerRes, fragmentSalary).commit();
    }

    public void switchToImportance(){
        FragmentImportance fragmentImportance = new FragmentImportance();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(mContainerRes, fragmentImportance).commit();
    }

    public void switchToStock(){
        FragmentStock fragmentStock = new FragmentStock();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(mContainerRes, fragmentStock).commit();
    }

    public void switchToOverview(){
        FragmentOverview fragmentOverview = new FragmentOverview();
        FragmentManager fragmentManager = getSupportFragmentManager();
        setActionBarTitle("Account Overview");
        fragmentManager.beginTransaction().replace(mContainerRes, fragmentOverview).commit();
    }

    public void switchToInvestment(){
        Intent i = new Intent(this, InvestmentActivity.class);
        startActivity(i);
    }

    public void switchToSignIn(){
        Intent i = new Intent(this, SigninActivity.class);
        startActivity(i);
    }



    public void setActionBarTitle(String title){
        setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }




}