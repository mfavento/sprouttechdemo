package studiocephei.sprouttech.AccountFragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import studiocephei.sprouttech.AccountActivity;
import studiocephei.sprouttech.Constants;
import studiocephei.sprouttech.InvestmentActivity;
import studiocephei.sprouttech.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 */
public class FragmentAccImage extends Fragment {

    ImageView mImageView;

    Bitmap mBitSurge;
    AccountActivity mAccountActivity;



    public FragmentAccImage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_accimage, container, false);
        mImageView = (ImageView)v.findViewById(R.id.iv_sprout);
        mBitSurge = Constants.decodeSampledBitmapFromResource(getResources(),R.drawable.surge_sprout, 400, 400);
        mAccountActivity = (AccountActivity) getActivity();
        mImageView.setImageBitmap(mBitSurge);

        //get data from investment activity
        return v;
    }




    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public static FragmentAccImage newInstance() {
        FragmentAccImage fragmentAccImage = new FragmentAccImage();
        return fragmentAccImage;
    }


}
